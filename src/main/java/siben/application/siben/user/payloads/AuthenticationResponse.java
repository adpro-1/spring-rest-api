package siben.application.siben.user.payloads;

public class AuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private String username;

    public AuthenticationResponse(String accessToken, String username) {
        this.setAccessToken(accessToken);
        this.setUsername(username);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
}
