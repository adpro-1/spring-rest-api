package siben.application.siben.user.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;

public class RegisterRequest {

    private final int minName = 3;
    private final int maxName = 40;
    private final int minPassword = 6;
    private final int maxPassword = 40;
    private final int maxUsername = 30;

    @NotBlank
    @Size(min = minName, max = maxName)
    private String name;

    @NotBlank
    @Size(min = minName, max = maxUsername)
    private String username;

    @NotBlank
    @Size(max = maxName)
    @Email
    private String email;

    @NotBlank
    @Size(min = minPassword, max = maxPassword)
    private String password;

    public String getName() {
        return name;
    }

    public RegisterRequest(String name, String username, String password, String email) {
        this.setName(name);
        this.setEmail(email);
        this.setPassword(password);
        this.setUsername(username);
    }

    public RegisterRequest(){}

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
