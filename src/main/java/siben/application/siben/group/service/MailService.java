package siben.application.siben.group.service;

import java.io.IOException;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;

public interface MailService {
    void sendRequestBudget(User user, Group group, Long amount) throws IOException;
    void sendRequestMember(User user, Group group) throws IOException;
    void sendTuition(Tuition tuition) throws IOException;
    void sendReportPayTuition(MemberTuition memberTuition) throws IOException;
    void sendReportVerifyTuition(MemberTuition memberTuition) throws IOException;
}
