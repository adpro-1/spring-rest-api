package siben.application.siben.group.service;

import java.io.IOException;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;

import org.springframework.stereotype.Service;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;

@Service
public class MailServiceImpl implements MailService {
    private final int lastIndex = 3;

    private void sendMail(Email from, String templateId, Personalization personalization, String subject) throws IOException {
        Mail mail = new Mail();
        mail.setFrom(from);
        mail.setSubject(subject);
        mail.setTemplateId(templateId);
        mail.addPersonalization(personalization);
        SendGrid sg = new SendGrid("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");
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch(IOException ex) {
            throw ex;
        }
    }

    @Override
    public void sendRequestBudget(User user, Group group, Long amount) throws IOException {
        Email from = new Email(user.getUsername() + "@noano.tech");
        String templateId = "d-121f8c5e9f48468c803f4c58deae567d";
        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("group", group.getName());
        personalization.addDynamicTemplateData("email", user.getEmail());
        personalization.addDynamicTemplateData("amount", Long.toString(amount));
        String subject = "Budget Request for group " + group.getName();
        for (User u : group.getAdmins()) {
            personalization.addTo(new Email(u.getEmail()));
        }
        this.sendMail(from, templateId, personalization, subject);
    }

    @Override
    public void sendRequestMember(User user, Group group) throws IOException {
        Email from = new Email(user.getUsername() + "@noano.tech");
        String templateId = "d-e89fc20943ed4287b7d046a26cc46d33";
        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("group", group.getName());
        personalization.addDynamicTemplateData("name", user.getName());
        personalization.addDynamicTemplateData("email", user.getEmail());
        String subject = "Membership request for group " + group.getName();
        for (User u : group.getAdmins()) {
            personalization.addTo(new Email(u.getEmail()));
        }
        this.sendMail(from, templateId, personalization, subject);
    }

    @Override
    public void sendTuition(Tuition tuition) throws IOException {
        // TODO Auto-generated method stub
        Email from = new Email(tuition.getGroup().getName() + "@noano.tech");
        String templateId = "d-1b626f991ed74cb98a1063130af11da3";
        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("group", tuition.getGroup().getName());
        personalization.addDynamicTemplateData("amount", Long.toString(tuition.getNominal()));
        personalization.addDynamicTemplateData("tuition", tuition.getTitle());
        String subject = "It's time to pay your tuiiton :)";
        for (MemberTuition mt : tuition.getMemberTuitions()){
            if (!mt.getIsPaid()) {
                personalization.addTo(new Email(mt.getMember().getEmail()));
            }
        }
        this.sendMail(from, templateId, personalization, subject);
    }

    @Override
    public void sendReportPayTuition(MemberTuition memberTuition) throws IOException {
        Email from = new Email(memberTuition.getMember().getUsername() + "@noano.tech");
        String templateId = "d-69fc4bbdcb3c4d008a3dd2945c14c117";
        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("email", memberTuition.getMember().getEmail());
        personalization.addDynamicTemplateData("amount", Long.toString(memberTuition.getTuition().getNominal()));
        personalization.addDynamicTemplateData("tuition", memberTuition.getTuition().getTitle());
        personalization.addDynamicTemplateData("group", memberTuition.getTuition().getGroup().getName());
        for (User u : memberTuition.getTuition().getGroup().getAdmins()) {
            personalization.addTo(new Email(u.getEmail()));
        }
        String subject = "Payment report from " + memberTuition.getMember().getEmail();
        this.sendMail(from, templateId, personalization, subject);
    }

    @Override
    public void sendReportVerifyTuition(MemberTuition memberTuition) throws IOException {
        Email from = new Email(memberTuition.getTuition().getGroup().getName() + "@noano.tech");
        String templateId = "d-66c545e4c4e04a61aaabc95d252ce4a5";
        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("email", memberTuition.getMember().getEmail());
        personalization.addDynamicTemplateData("group", memberTuition.getTuition().getGroup().getName());
        personalization.addDynamicTemplateData("tuition", memberTuition.getTuition().getTitle());
        personalization.addTo(new Email(memberTuition.getMember().getEmail()));
        String subject = "Payment verification";
        this.sendMail(from, templateId, personalization, subject);
    }

}
