package siben.application.siben.group.service;

import java.util.List;
import java.util.Set;
import siben.application.siben.group.core.Anggota;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MembershipRequest;
import siben.application.siben.model.group.RequestBudget;


public interface GroupService {

    void setUp(Group group);
    List<Anggota> getAllMember();
    List<Anggota> getAllAdmin();
    Group getGroup();
    Boolean cekIsAdmin(Anggota user);
    Boolean cekIsMember(Anggota user);
    Set<MembershipRequest> getMembershipRequest(Group group);
    Set<RequestBudget> getRequestBudget(Group group);
}
