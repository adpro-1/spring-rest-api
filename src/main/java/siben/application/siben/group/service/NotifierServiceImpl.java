package siben.application.siben.group.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import siben.application.siben.group.repository.RequestBudgetRepository;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.RequestBudget;
import siben.application.siben.model.user.User;

@Service
public class NotifierServiceImpl implements NotifierService {

    @Autowired
    private RequestBudgetRepository requestBudgetRepository;

    @Autowired
    private MailService mailService;

    @Override
    public void requestBudget(User user, Group group, Long amount) throws IOException {
        // TODO Auto-generated method stub
        RequestBudget requestBudget = new RequestBudget(user, group, 0, amount);
        Set<RequestBudget> reqBudgetSet = new HashSet<>();
        reqBudgetSet.add(requestBudget);
        user.setRequestBudget(reqBudgetSet, group);
        requestBudgetRepository.save(requestBudget);
        mailService.sendRequestBudget(user, group, amount);
    }

    @Override
    public List<RequestBudget> getAllRequestBudget() {
        // TODO Auto-generated method stub
        return requestBudgetRepository.findAll();
    }

    @Override
    public Optional<RequestBudget> getRequestById(Long id) {
        // TODO Auto-generated method stub
        return requestBudgetRepository.findById(id);
    }

    @Override
    public void requestMembership(User user, Group group) throws IOException {
        mailService.sendRequestMember(user, group);
    }

    @Override
    public void reportPayment(MemberTuition memberTuition) throws IOException {
        mailService.sendReportPayTuition(memberTuition);
    }

    @Override
    public void reportVerifPayment(MemberTuition memberTuition) throws IOException {
        mailService.sendReportVerifyTuition(memberTuition);
    }
}
