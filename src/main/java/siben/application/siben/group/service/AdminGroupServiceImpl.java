package siben.application.siben.group.service;

import java.util.HashSet;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;
import siben.application.siben.group.core.Member;
import siben.application.siben.group.repository.GroupRepository;
import siben.application.siben.group.repository.TuitionRepository;

@Service
public class AdminGroupServiceImpl implements AdminGroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TuitionRepository tuitionRepository;

    @Autowired
    private MailService mailService;

    @Override
    public void createGroup(User user, Group group) {
        group.setCreator(user);
        group.getAdmins().add(user);
        group.getMembers().add(user);
        groupRepository.save(group);
    }

    @Override
    public void deleteGroup(Group group) {
        groupRepository.delete(group);
    }

    @Override
    public void addUser(Member member, Group group) {
        group.getMembers().add(member.getUser());
        groupRepository.save(group);
    }

    @Override
    public void removeUser(Member member, Group group) {
        group.getMembers().remove(member.getUser());
        group.getAdmins().remove(member.getUser());
        groupRepository.save(group);
    }

    @Override
    public List<Group> getAllGroup(User user) {
        return groupRepository.allGroup(user.getId());
    }

    @Override
    public List<User> getListUser(Group group) {
        return groupRepository.allMemberGroup(group.getId());
    }

    @Override
    public void addAdmin(Member member, Group group) {
        group.getAdmins().add(member.getUser());
        groupRepository.save(group);
    }

    @Override
    public void addTuition(Tuition tuition, Group group){
        group.getTuition().add(tuition);
        groupRepository.save(group);
    }

    @Override
    public void notifyTuition(Tuition tuition, Group group) throws IOException{
        if (!tuition.getIsNotified()) {
            this.seedMemberTuition(tuition, group);
        }
        mailService.sendTuition(tuition);
    }

    private void seedMemberTuition(Tuition tuition, Group group) {
        Set<MemberTuition> memberTuition = new HashSet<>();
        for (User member : group.getMembers()){
            MemberTuition newMemberTuition = new MemberTuition(tuition,member);
            memberTuition.add(newMemberTuition);
        }
        tuition.setMemberTuition(memberTuition);
        tuition.setIsNotified(true);
        tuitionRepository.save(tuition);
    }

    @Override
    public void editBudget(Group group, BigInteger newBudget) {
        group.setBudget(newBudget);
        groupRepository.save(group);
    }
}
