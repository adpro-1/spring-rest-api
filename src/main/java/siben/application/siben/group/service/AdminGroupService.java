package siben.application.siben.group.service;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import siben.application.siben.model.user.User;
import siben.application.siben.group.core.Member;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.Tuition;

public interface AdminGroupService {
    void createGroup(User user, Group group);
    void deleteGroup(Group group);
    void addUser(Member member, Group group);
    void removeUser(Member member, Group group);
    void addAdmin(Member member, Group group);
    void addTuition(Tuition tuition, Group group);
    void notifyTuition(Tuition tuition, Group group) throws IOException;
    void editBudget(Group group, BigInteger newBudget);
    List<Group> getAllGroup(User user);
    List<User> getListUser(Group group);
}
