package siben.application.siben.group.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MembershipRequest;
import siben.application.siben.model.group.RequestBudget;
import siben.application.siben.model.user.User;
import siben.application.siben.group.core.Admin;
import siben.application.siben.group.core.Anggota;
import siben.application.siben.group.core.GroupCore;
import siben.application.siben.group.core.Member;
import siben.application.siben.group.repository.GroupRepository;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    private GroupCore groupCore;

    public void setUp(Group group){
        this.groupCore = new GroupCore(group);
        this.addMemberHelper(group.getAdmins(),"admin");
        this.addMemberHelper(group.getMembers(), "member");
    }

    public void addMemberHelper(Set<User> users, String type) {
        for(User user : users) {
            if (type.equals("admin")){
                this.groupCore.addAdmins(new Admin(user));
            }else {
                this.groupCore.addMember(new Member(user));
            }
        }
    }

    @Override
    public List<Anggota> getAllMember() {
        return this.groupCore.getAllMember();
    }

    @Override
    public List<Anggota> getAllAdmin() {
        return this.groupCore.getAllAdmin();
    }

    public Group getGroup(){
        return this.groupCore.getGroup();
    }

    @Override
    public Set<MembershipRequest> getMembershipRequest(Group group) {
        return group.getMembershipRequest();
    }

    @Override
    public Set<RequestBudget> getRequestBudget(Group group) {
        return group.getRequestBudget();
    }

    @Override
    public Boolean cekIsAdmin(Anggota user){
        for (Anggota admin : this.getAllAdmin()){
            if (admin.compareTo(user) == 0){
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean cekIsMember(Anggota user) {
        for (Anggota member : this.getAllMember()){
            if (member.compareTo(user) == 0){
                return true;
            }
        }
        return false;
    }
}
