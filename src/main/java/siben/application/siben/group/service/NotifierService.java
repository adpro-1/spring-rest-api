package siben.application.siben.group.service;

import java.io.IOError;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.RequestBudget;
import siben.application.siben.model.user.User;

public interface NotifierService {
    void requestBudget(User user, Group group, Long amount) throws IOException;
    void requestMembership(User user, Group group) throws IOException;
    void reportPayment(MemberTuition memberTuition) throws IOException;
    void reportVerifPayment(MemberTuition memberTuition) throws IOException;
    List<RequestBudget> getAllRequestBudget() throws IOError;
    Optional<RequestBudget> getRequestById(Long id);
}
