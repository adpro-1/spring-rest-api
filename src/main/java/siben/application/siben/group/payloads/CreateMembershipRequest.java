package siben.application.siben.group.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateMembershipRequest {
    private final int minSize = 4;
    private final int maxSize = 40;
    private final int uniqueCodeRange = 20;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String username;

    @NotBlank
    @Size(min = minSize, max = uniqueCodeRange)
    private String code;

    public CreateMembershipRequest(String username, String code) {
        this.setUsername(username);
        this.setCode(code);
    }

    public CreateMembershipRequest(){}

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

}
