package siben.application.siben.group.payloads;

import java.math.BigInteger;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UpdateGroupBudget {
    private final int minSize = 4;
    private final int maxSize = 40;

    private BigInteger budget;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String username;

    UpdateGroupBudget(BigInteger budget, String username){
        this.setBudget(budget);
        this.setUsername(username);
    }

    UpdateGroupBudget(){}

    public void setBudget(BigInteger budget) {
        this.budget = budget;
    }

    public BigInteger getBudget() {
        return this.budget;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }
}
