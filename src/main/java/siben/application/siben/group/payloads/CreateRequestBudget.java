package siben.application.siben.group.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateRequestBudget {
    private final int minSize = 4;
    private final int maxSize = 40;
    private final int uniqueCodeRange = 20;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String username;

    @NotBlank
    @Size(min = minSize, max = uniqueCodeRange)
    private String code;

    @NotBlank
    private String amount;

    public CreateRequestBudget(String username, String code, String amount) {
        this.setUsername(username);
        this.setCode(code);
        this.setAmount(amount);
    }

    public CreateRequestBudget() {

    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public Long getAmount() {
        return Long.parseLong(amount);
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
