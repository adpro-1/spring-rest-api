package siben.application.siben.group.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class IdRequest {
    private final int minSize = 4;
    private final int maxSize = 40;

    private Long id;

    private int status = 0;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String username;

    public IdRequest(Long id, String username){
        this.setId(id);
        this.setUsername(username);
    }

    public IdRequest(Long id, String username, int status){
        this.setId(id);
        this.setUsername(username);
        this.setStatus(status);
    }

    public IdRequest(){

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;
    }
}
