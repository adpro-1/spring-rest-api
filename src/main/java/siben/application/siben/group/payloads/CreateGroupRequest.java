package siben.application.siben.group.payloads;

import java.math.BigInteger;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import siben.application.siben.group.config.StringGenerator;

public class CreateGroupRequest {
    private final int minSize = 4;
    private final int maxSize = 40;
    private final int uniqueCodeRange = 20;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String name;

    private BigInteger budget;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String username;

    private String uniqueCode;

    public CreateGroupRequest(String name, BigInteger budget, String username) {
        this.setName(name);
        this.setBudget(budget);
        this.setUsername(username);
        this.setUniqueCode();
    }

    public CreateGroupRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getBudget() {
        return this.budget;
    }

    public void setBudget(BigInteger budget) {
        this.budget = budget;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUniqueCode() {
        StringGenerator code = new StringGenerator();
        this.uniqueCode = code.randomAlphaNumeric(uniqueCodeRange);
    }

    public String getUniqueCode() {
        return this.uniqueCode;
    }
}
