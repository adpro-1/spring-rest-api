package siben.application.siben.group.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateTuitionRequest {
    private final int minSize = 4;
    private final int maxSize = 200;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String username;

    @NotBlank
    @Size(min = minSize, max = maxSize)
    private String title;

    private Long nominal;

    private Long groupId;

    public CreateTuitionRequest(){

    }

    public CreateTuitionRequest(String username, String title, Long nominal, Long groupId) {
        this.setGroupId(groupId);
        this.setNominal(nominal);
        this.setTitle(title);
        this.setUsername(username);
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public Long getNominal(){
        return this.nominal;
    }

    public void setNominal(Long nominal) {
        this.nominal = nominal;
    }

    public void setGroupId(Long groupId){
        this.groupId = groupId;
    }

    public Long getGroupId() {
        return this.groupId;
    }
}
