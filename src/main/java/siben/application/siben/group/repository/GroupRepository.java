package siben.application.siben.group.repository;

import siben.application.siben.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import siben.application.siben.model.group.Group;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

    Group findByCode(String code);

    Optional<Group> findById(Long id);

    @Query(value = "SELECT * FROM users u WHERE u.id IN (SELECT user_id FROM  group_admins WHERE group_id = ?1 )", nativeQuery = true)
    List<User> allAdminGroup(Long groupId);

    @Query(value = "SELECT * FROM users u WHERE u.id IN (SELECT user_id FROM  group_members WHERE group_id = ?1 )", nativeQuery = true)
    List<User> allMemberGroup(Long groupId);

    @Modifying
    @Query(value = "INSERT INTO group_members (group_id, user_id) VALUES(?1, ?2)", nativeQuery = true)
    @Transactional
    void addMember(Long groupId, Long userId);

    @Modifying
    @Query(value = "INSERT INTO group_admins (group_id, user_id) VALUES(?1, ?2)", nativeQuery = true)
    @Transactional
    void addAdmin(Long groupId, Long userId);

    @Modifying
    @Query(value = "INSERT INTO group_creator (group_id, user_id) VALUES(?1, ?2)", nativeQuery = true)
    @Transactional
    void addCreator(Long groupId, Long userId);

    @Query(value = "select DISTINCT count(user_id) > 0 from group_admins where user_id = ?1 and group_id = ?2", nativeQuery = true)
    Boolean isAdmin(Long userId, Long groupId);

    @Query(value = "select DISTINCT count(um.user_id) > 0 from user_member_request um, requested_group_membership ug where um.user_id = ?1 and ug.group_id = ?2", nativeQuery = true)
    Boolean isAlreadyRequested(Long userId, Long groupId);

    @Modifying
    @Query(value = "UPDATE groups SET budget = ?2 WHERE id = ?1", nativeQuery = true)
    @Transactional
    void setBudget(Long groupId, BigInteger budget);

    @Query(value = "SELECT * FROM groups WHERE id IN (SELECT group_id FROM  group_members WHERE user_id= ?1 )", nativeQuery = true)
    @Transactional
    List<Group> allGroup(Long userId);

    @Query(value = "SELECT * FROM groups g WHERE g.id IN (SELECT group_id FROM  group_creator WHERE user_id = ?1 )", nativeQuery = true)
    List<Group> allGroupCreated(Long userId);

    @Query(value = "SELECT * FROM groups g WHERE g.id IN (SELECT group_id FROM group_admins WHERE user_id = ?1)", nativeQuery = true)
    List<Group> allGroupAdmin(Long userId);
}
