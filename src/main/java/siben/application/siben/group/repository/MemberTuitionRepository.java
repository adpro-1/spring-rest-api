package siben.application.siben.group.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import siben.application.siben.model.group.MemberTuition;

@Repository
public interface MemberTuitionRepository extends JpaRepository<MemberTuition, Long> {

}
