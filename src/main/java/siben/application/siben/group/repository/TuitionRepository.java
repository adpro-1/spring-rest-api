package siben.application.siben.group.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import siben.application.siben.model.group.Tuition;

@Repository
public interface TuitionRepository extends JpaRepository<Tuition, Long> {

}
