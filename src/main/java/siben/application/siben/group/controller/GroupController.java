package siben.application.siben.group.controller;

import org.springframework.web.bind.annotation.RestController;

import siben.application.siben.group.config.StringGenerator;
import siben.application.siben.group.core.Admin;
import siben.application.siben.group.core.Member;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.MembershipRequest;
import siben.application.siben.model.group.RequestBudget;
import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;
import siben.application.siben.group.payloads.CreateGroupRequest;
import siben.application.siben.group.payloads.CreateMembershipRequest;
import siben.application.siben.group.payloads.CreateRequestBudget;
import siben.application.siben.group.payloads.CreateTuitionRequest;
import siben.application.siben.group.payloads.IdRequest;
import siben.application.siben.group.payloads.UpdateGroupBudget;
import siben.application.siben.group.repository.GroupRepository;
import siben.application.siben.group.repository.MemberTuitionRepository;
import siben.application.siben.group.repository.MembershipRequestRepository;
import siben.application.siben.group.repository.RequestBudgetRepository;
import siben.application.siben.group.repository.TuitionRepository;
import siben.application.siben.group.service.AdminGroupService;
import siben.application.siben.group.service.GroupService;
import siben.application.siben.group.service.NotifierService;
import siben.application.siben.user.repository.UserRepository;

import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(path = "/api/group")
public class GroupController {
    private final int uniqueCodeRange = 20;
    private final int rejectStatus = 1;

    @Autowired
    private RequestBudgetRepository requestBudgetRepository;

    @Autowired
    private NotifierService notifierService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private AdminGroupService adminGroupService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private TuitionRepository tuitionRepository;

    @Autowired
    private MemberTuitionRepository memberTuitionRepository;

    @Autowired
    private MembershipRequestRepository membershipRequestRepository;

    @RequestMapping(path = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserGroup(@PathVariable String username) {
        User user = userRepository.findByUsername(username).get();
        HashMap<String, Object> response = new HashMap<String, Object>();
        response.put("groups", user.getUserGroups());
        response.put("tuition", user.getMemberTuition());
        return ResponseEntity.ok().<HashMap<String, Object>>body(response);
    }

    @RequestMapping(path = "/delete/{groupId}/{username}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteGroup(@PathVariable Long groupId, @PathVariable String username) {
        User user = userRepository.findByUsername(username).get();
        Group group = groupRepository.findById(groupId).get();
        Admin admin = new Admin(user);
        groupService.setUp(group);
        if (groupService.cekIsAdmin(admin)) {
            adminGroupService.deleteGroup(group);
            return ResponseEntity.ok().body("Success delete group " + group.getName());
        }
        return ResponseEntity.ok().body("Only admin can delete group");
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> createGroup(@Valid @RequestBody CreateGroupRequest request) {
        StringGenerator code = new StringGenerator();
        User user = userRepository.findByUsername(request.getUsername()).get();
        Group group = new Group(request.getName(), code.randomAlphaNumeric(uniqueCodeRange), request.getBudget());
        adminGroupService.createGroup(user, group);
        Set<Group> userGroups = user.getUserGroups();
        userGroups.add(group);
        return ResponseEntity.ok().body(userGroups);
    }

    @PostMapping("/request-membership")
    public ResponseEntity<?> createMembershipRequest(@Valid @RequestBody CreateMembershipRequest request) throws IOException {
        User user = userRepository.findByUsername(request.getUsername()).get();
        Group group = groupRepository.findByCode(request.getCode());
        MembershipRequest membershipRequest = new MembershipRequest(user, group, 0);
        if (!groupRepository.isAlreadyRequested(user.getId(), group.getId())) {
            user.setMembershipRequest(Collections.singleton(membershipRequest));
            group.setMembershipRequest(Collections.singleton(membershipRequest));
            membershipRequestRepository.save(membershipRequest);
            notifierService.requestMembership(user, group);
            return ResponseEntity.ok().body(user.getMembershipRequest());
        }
        return ResponseEntity.ok().body(user.getMembershipRequest());
    }

    @PostMapping("/request-budget")
    public ResponseEntity<?> createRequestBudget(@Valid @RequestBody CreateRequestBudget request) throws IOException {
        String reqCreator = request.getUsername();
        String reqCode = request.getCode();
        Long reqAmount = request.getAmount();
        User user = userRepository.findByUsername(reqCreator).get();
        Group group = groupRepository.findByCode(reqCode);
        groupService.setUp(group);
        if (groupService.cekIsMember(new Member(user))){
            notifierService.requestBudget(user, group, reqAmount);
            return ResponseEntity.ok().body(groupService.getRequestBudget(group));
        }
        return ResponseEntity.badRequest().body("Only member can request budget");
    }

    @PostMapping("/approve-membership")
    public ResponseEntity<?> approveMembershipRequest(@Valid @RequestBody IdRequest request) {
        User user = userRepository.findByUsername(request.getUsername()).get();
        Admin admin = new Admin(user);
        MembershipRequest membershipRequest = membershipRequestRepository.findById(request.getId()).get();
        Group group = membershipRequest.getGroup();
        groupService.setUp(group);
        if (groupService.cekIsAdmin(admin)) {
            User requester = membershipRequest.getUser();
            Member newMember = new Member(requester);
            adminGroupService.addUser(newMember, group);
            membershipRequest.setStatus(request.getStatus());
            membershipRequestRepository.save(membershipRequest);
            if (request.getStatus() == 2) {
                return ResponseEntity.ok().body("Success add new member");
            }
            return ResponseEntity.ok().body("Success reject new member");
        } else {
            return ResponseEntity.badRequest().body("Add member need admin role");
        }
    }

    @RequestMapping(path = "/detail/request/{groupId}/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> getGroupRequest(@PathVariable Long groupId, @PathVariable String username)
            throws JsonProcessingException {
        User user = userRepository.findByUsername(username).get();
        Group group = groupRepository.findById(groupId).get();
        groupService.setUp(group);
        Member member = new Member(user);
        if (groupService.cekIsAdmin(member)) {
            HashMap<String, Object> response = new HashMap<String, Object>();
            response.put("membershipRequest", groupService.getMembershipRequest(group));
            response.put("requestBudget", groupService.getRequestBudget(group));
            response.put("tuition", group.getTuition());
            return ResponseEntity.ok().<HashMap<String,Object>>body(response);
        }
        return ResponseEntity.ok().body(groupService.getRequestBudget(group));
    }

    @RequestMapping(path = "/detail/{groupId}/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> getGroupDetail(@PathVariable Long groupId, @PathVariable String username){
        User user = userRepository.findByUsername(username).get();
        Group group = groupRepository.findById(groupId).get();
        groupService.setUp(group);
        Member member = new Member(user);
        if (groupService.cekIsMember(member)) {
            return ResponseEntity.ok().body(group);
        }
        return ResponseEntity.ok().body("Only Member can get the data");
    }

    @RequestMapping(path="/detail/remove-member/{username}/{groupId}/{usernameToRemove}", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeMember(@PathVariable String username, @PathVariable Long groupId, @PathVariable String usernameToRemove){
        User user = userRepository.findByUsername(username).get();
        User userToRemove = userRepository.findByUsername(usernameToRemove).get();
        Group group = groupRepository.findById(groupId).get();
        groupService.setUp(group);
        Admin admin = new Admin(user);
        Member member = new Member(userToRemove);
        if (groupService.cekIsAdmin(admin)) {
            adminGroupService.removeUser(member, group);
            return ResponseEntity.ok().body("Success remove " + userToRemove.getUsername());
        }
        return ResponseEntity.ok().body("Only Admin can remove member");
    }

    @PostMapping("/approve-request-budget")
    public ResponseEntity<?> approveRequestBudget(@Valid @RequestBody IdRequest request) {
        User user = userRepository.findByUsername(request.getUsername()).get();
        Admin admin = new Admin(user);
        RequestBudget requestBudget = requestBudgetRepository.findById(request.getId()).get();
        Group group = requestBudget.getGroup();
        groupService.setUp(group);
        if (groupService.cekIsAdmin(admin)) {
            User requester = requestBudget.getUser();
            BigInteger budget = BigInteger.valueOf(requestBudget.getNominal());
            BigInteger groupBudget = group.getBudget().subtract(budget);
            if (groupBudget.intValue() >= 0) {
                requestBudget.setStatus(request.getStatus());
            } else {
                requestBudget.setStatus(rejectStatus);
            }
            requestBudgetRepository.save(requestBudget);
            if (requestBudget.getStatus() == 2){
                group.setBudget(groupBudget);
                groupRepository.save(group);
                userRepository.save(requester);
                return ResponseEntity.ok().body("Request Budget accepted");
            }
            return ResponseEntity.ok().body("Request Budget rejected");
        } else {
            return ResponseEntity.badRequest().body("Invalid role");
        }
    }
    @RequestMapping(path="/detail/invite-member/{username}/{groupId}/{usernameInvite}", method = RequestMethod.POST)
    public ResponseEntity<?> inviteMember(@PathVariable Long groupId, @PathVariable String username, @PathVariable String usernameInvite){
        User user = userRepository.findByUsername(username).get();
        User userInvite = userRepository.findByUsername(usernameInvite).get();
        Group group = groupRepository.findById(groupId).get();
        groupService.setUp(group);
        Admin admin = new Admin(user);
        Member member = new Member(userInvite);
        if(groupService.cekIsAdmin(admin)) {
            if(!groupService.getAllMember().contains(member)){
                adminGroupService.addUser(member, group);
                return ResponseEntity.ok().body("Success invite " + userInvite.getUsername());
            }
            return ResponseEntity.ok().body("Member already Exists");
        }
        return ResponseEntity.ok().body("Only Admin can invite member");
    }

    @RequestMapping(path="/detail/invite-admin/{username}/{groupId}/{usernameInvite}", method = RequestMethod.POST)
    public ResponseEntity<?> inviteAdmin(@PathVariable Long groupId, @PathVariable String username, @PathVariable String usernameInvite){
        User user = userRepository.findByUsername(username).get();
        User userInvite = userRepository.findByUsername(usernameInvite).get();
        Group group = groupRepository.findById(groupId).get();
        groupService.setUp(group);
        Admin admin = new Admin(user);
        Member member = new Member(userInvite);
        if(groupService.cekIsAdmin(admin)) {
            if(!groupService.getAllAdmin().contains(new Admin(userInvite))){
                if(!groupService.getAllMember().contains(member)){
                    adminGroupService.addUser(member, group);
                }
                adminGroupService.addAdmin(member, group);
                return ResponseEntity.ok().body("Success add new admin: " + userInvite.getUsername());
            }
            return ResponseEntity.ok().body("Member already an admin");
        }
        return ResponseEntity.ok().body("Only Admin can invite member");
    }

    @PostMapping("/detail/create-tuition")
    public ResponseEntity<?> createTuition(@Valid @RequestBody CreateTuitionRequest request) {
        User user = userRepository.findByUsername(request.getUsername()).get();
        Group group = groupRepository.findById(request.getGroupId()).get();
        Tuition tuition = new Tuition(request.getTitle(), request.getNominal(), group);
        groupService.setUp(group);
        if (groupService.cekIsAdmin(new Admin(user))) {
            adminGroupService.addTuition(tuition, group);
            return ResponseEntity.ok().body("Success create new tuition for group " + group.getName());
        }
        return ResponseEntity.badRequest().body("Only admin can make a tuition");
    }

    @PostMapping("/detail/notify-tuition")
    public ResponseEntity<?> notifyTuition(@Valid @RequestBody IdRequest request) throws IOException {
        User user = userRepository.findByUsername(request.getUsername()).get();
        Tuition tuition = tuitionRepository.findById(request.getId()).get();
        Group group = tuition.getGroup();
        groupService.setUp(group);
        if (groupService.cekIsAdmin(new Admin(user))){
            adminGroupService.notifyTuition(tuition, group);
            return ResponseEntity.ok().body("Success notify tuition to all member");
        }
        return ResponseEntity.badRequest().body("Only admin can notify tuition to all member");
    }

    @PatchMapping("/detail/paid-tuition")
    public ResponseEntity<?> paidTuition(@Valid @RequestBody IdRequest request) throws IOException {
        User user = userRepository.findByUsername(request.getUsername()).get();
        MemberTuition memberTuition = memberTuitionRepository.findById(request.getId()).get();
        Tuition tuition = memberTuition.getTuition();
        Group group = tuition.getGroup();
        groupService.setUp(group);
        if (groupService.cekIsMember(new Member(user)) && user.getUsername() == memberTuition.getMember().getUsername() && !memberTuition.getIsVerified()) {
            memberTuition.setIsPaid(!memberTuition.getIsPaid());
            memberTuition.setCreatedDateTime(LocalDateTime.now());
            memberTuitionRepository.save(memberTuition);
            if (memberTuition.getIsPaid()) {
                notifierService.reportPayment(memberTuition);
            }
            return ResponseEntity.ok().body("Success paid group tuition");
        }
        return ResponseEntity.badRequest().body("Only member which is have the tuition can pay the tuition");
    }

    @PatchMapping("/detail/verify-paid-tuition")
    public ResponseEntity<?> verifyPaidTuition(@Valid @RequestBody IdRequest request) throws IOException {
        User user = userRepository.findByUsername(request.getUsername()).get();
        MemberTuition memberTuition = memberTuitionRepository.findById(request.getId()).get();
        Tuition tuition = memberTuition.getTuition();
        Group group = tuition.getGroup();
        groupService.setUp(group);
        if (groupService.cekIsAdmin(new Admin(user))){
            memberTuition.setIsVerified(!memberTuition.getIsVerified());
            memberTuitionRepository.save(memberTuition);
            if (memberTuition.getIsVerified()) {
                notifierService.reportVerifPayment(memberTuition);
            }
            return ResponseEntity.ok().body("Success verify member tuition");
        }
        return ResponseEntity.badRequest().body("Only member which is have the tuition can pay the tuition");
    }

    @RequestMapping(path="/detail/{username}/tuition/{tuitionId}", method = RequestMethod.POST)
    public ResponseEntity<?> tuitionDetail(@PathVariable String username, @PathVariable Long tuitionId){
        User user = userRepository.findByUsername(username).get();
        Tuition tuition = tuitionRepository.findById(tuitionId).get();
        Group group = tuition.getGroup();
        groupService.setUp(group);
        if (groupService.cekIsAdmin(new Admin(user))) {
            return ResponseEntity.ok().body(tuition);
        }
        return ResponseEntity.badRequest().body("Only member can see the data");
    }

    @PatchMapping(path="/detail/edit-budget/{groupId}")
    public ResponseEntity<?> editBudget(@Valid @RequestBody UpdateGroupBudget updateGroupBudget, @PathVariable("groupId") Long groupId){
        User user = userRepository.findByUsername(updateGroupBudget.getUsername()).get();
        Group group = groupRepository.findById(groupId).get();
        groupService.setUp(group);
        Member member = new Member(user);
        if (groupService.cekIsAdmin(member)) {
            BigInteger oldBudget = group.getBudget();
            BigInteger addBudget = updateGroupBudget.getBudget();
            BigInteger sum = addBudget.add(oldBudget);

            adminGroupService.editBudget(group, sum);
            groupRepository.save(group);
            return ResponseEntity.ok().body("Group budget already updated");
        }
        return ResponseEntity.ok().body("Only Admin can edit budget");
    }
}
