package siben.application.siben.group.core;

import java.util.ArrayList;
import java.util.List;

import siben.application.siben.model.group.Group;


public class GroupCore {

    private Group group;
    private List<Anggota> members = new ArrayList<>();
    private List<Anggota> admins = new ArrayList<>();

    public GroupCore(Group group) {
        this.group = group;
    }

    public void addMember(Anggota members){
        this.members.add(members);
    }

    public List<Anggota> getAllMember(){
        return this.members;
    }

    public void addAdmins(Anggota members){
        this.admins.add(members);
    }

    public List<Anggota> getAllAdmin(){
        return this.admins;
    }

    public Group getGroup(){
        return this.group;
    }
}
