package siben.application.siben.group.core;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.user.User;

public interface Request {
    Group getGroup();
    User getUser();
}
