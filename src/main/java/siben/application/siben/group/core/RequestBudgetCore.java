package siben.application.siben.group.core;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.RequestBudget;
import siben.application.siben.model.user.User;

public class RequestBudgetCore implements Request {
    private Group group;
    private User user;
    private RequestBudget requestBudget;

    public RequestBudgetCore(RequestBudget reqBudget, Group group, User user) {
        this.group = group;
        this.user = user;
        this.requestBudget = reqBudget;
    }

    @Override
    public Group getGroup() {
        // TODO Auto-generated method stub
        return this.group;
    }

    public User getUser() {
        return user;
    }

    public RequestBudget getRequestBudget() {
        return requestBudget;
    }

    public void setRequestBudget(RequestBudget requestBudget) {
        this.requestBudget = requestBudget;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
