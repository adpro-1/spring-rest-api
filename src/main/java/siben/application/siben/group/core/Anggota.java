package siben.application.siben.group.core;
import siben.application.siben.model.user.User;

public abstract class Anggota implements Comparable<Anggota> {

    abstract User getUser();
    abstract String getName();
    abstract String getEmail();
    abstract String getUsername();
    public abstract String toString();
    public abstract int compareTo(Anggota o);
}
