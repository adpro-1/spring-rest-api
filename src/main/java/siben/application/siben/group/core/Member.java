package siben.application.siben.group.core;

import siben.application.siben.model.user.User;

public class Member extends Anggota {

    private User user;

    public Member(User user) {
        this.user = user;
    }

    @Override
    public User getUser() {
        return this.user;
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return user.getName();
    }

    @Override
    public String getEmail() {
        // TODO Auto-generated method stub
        return user.getEmail();
    }

    @Override
    public String getUsername() {
        // TODO Auto-generated method stub
        return user.getUsername();
    }

    @Override
    public String toString(){
        return this.getUsername();
    }

    @Override
    public int compareTo(Anggota o) {
        if (user.getId() == o.getUser().getId()){
            return 0;
        }
        return -1;
    }

}
