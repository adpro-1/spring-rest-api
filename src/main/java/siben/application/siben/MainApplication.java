package siben.application.siben;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import java.util.TimeZone;


// submodule configuration
@SpringBootApplication
// submodule jpa migrations
@EntityScan(basePackageClasses = {
MainApplication.class,
Jsr310JpaConverters.class
})

@EnableJpaRepositories
@EnableAutoConfiguration
public class MainApplication {

@PostConstruct
void init() {
TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
}
public static void main(String[] args) {
SpringApplication.run(MainApplication.class, args);
}

}

