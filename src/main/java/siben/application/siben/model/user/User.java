package siben.application.siben.model.user;

import org.hibernate.annotations.NaturalId;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.MembershipRequest;
import siben.application.siben.model.group.RequestBudget;

@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "username"
        }),
        @UniqueConstraint(columnNames = {
            "email"
        })
})

public class User implements Serializable  {

    private final int maxName = 40;
    private final int maxPassword = 100;
    private final int maxUsername = 15;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = maxName)
    private String name;

    @NotBlank
    @Size(max = maxUsername)
    private String username;

    @NaturalId
    @NotBlank
    @Size(max = maxName)
    @Email
    private String email;

    @NotBlank
    @Size(max = maxPassword)
    @JsonIgnore
    private String password;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "members", fetch = FetchType.EAGER)
    private Set<Group> userGroups = new HashSet<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "admins", fetch = FetchType.EAGER)
    private Set<Group> userAdmins = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<MembershipRequest> membershipRequest = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<RequestBudget> requestBudget = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
    private Set<MemberTuition> memberTuitions = new HashSet<>();

    @Column(name="budget", nullable = false)
    private BigInteger budget = BigInteger.ZERO;

    public User() {
    }

    public User(String name, String username, String email, String password) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Group> getUserGroups() {
        return userGroups;
    }

    public void setGroup(Set<Group> group) {
        this.userGroups = group;
    }

    public Set<Group> getUserAdmins() {
        return userAdmins;
    }

    public void setUserAdmin(Set<Group> group) {
        this.userAdmins = group;
    }

    public void setBudget(BigInteger newBudget) {
        this.budget = newBudget;
    }

    public BigInteger getBudget() {
        return budget;
    }

    public Set<MembershipRequest> getMembershipRequest() {
        Set<MembershipRequest> request = new HashSet<>();
        for (MembershipRequest r : this.membershipRequest) {
            if (r.getStatus() == 0) {
                request.add(r);
            }
        }
        return request;
    }

    public void setMembershipRequest(Set<MembershipRequest> request) {
        this.membershipRequest = request;
    }

    public Set<RequestBudget> getRequestBudget() {
        return requestBudget;
    }

    public void setRequestBudget(Set<RequestBudget> requestBudget, Group group) {
        this.requestBudget = requestBudget;
        group.setRequestBudget(requestBudget);
    }

    public Set<MemberTuition> getMemberTuition() {
        Set<MemberTuition> allTuition = new HashSet<>();
        for (MemberTuition mt : this.memberTuitions){
            if (!mt.getIsVerified()) {
                allTuition.add(mt);
            }
        }
        return allTuition;
    }

    public void setMemberTuition(Set<MemberTuition> memberTuitions){
        this.memberTuitions = memberTuitions;
    }
}
