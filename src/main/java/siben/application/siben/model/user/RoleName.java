package siben.application.siben.model.user;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
