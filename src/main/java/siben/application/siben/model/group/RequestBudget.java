package siben.application.siben.model.group;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.persistence.JoinColumn;

import org.hibernate.annotations.CreationTimestamp;

import siben.application.siben.model.user.User;

@Entity
@Table(name = "request_budget")
public class RequestBudget {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "user_request_budget",
      joinColumns =
        { @JoinColumn(name = "request_id", referencedColumnName = "id") },
      inverseJoinColumns =
        { @JoinColumn(name = "user_id", referencedColumnName = "id") })
    private User user;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "group_budget_request",
      joinColumns =
        { @JoinColumn(name = "request_id", referencedColumnName = "id") },
      inverseJoinColumns =
        { @JoinColumn(name = "group_id", referencedColumnName = "id") })
    private Group group;

    @Column(name = "nominal")
    private Long nominal;

    public RequestBudget(User user, Group group, int status, Long nominal) {
      this.setUser(user);
      this.setGroup(group);
      this.setStatus(0);
      this.setNominal(nominal);
    }

    public RequestBudget() {

    }

    // 0 => just sent, 1 => rejected, 2 => accepted
    @Column(name="status")
    private int status = 0;

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public int getStatus() {
      return status;
    }

    public void setStatus(int status) {
      if (status < 0 || status > 2) {
        throw new NumberFormatException("Undefined status catched");
      } else {
        this.status = status;
      }
    }

    public Group getGroup() {
      return this.group;
    }

    public void setGroup(Group group) {
      this.group = group;
    }

    public User getUser() {
      return this.user;
    }

    public void setUser(User user) {
      this.user = user;
    }

    public LocalDateTime getCreatedDateTime() {
      return this.createDateTime;
    }

    public void setCreatedDateTime(LocalDateTime date) {
      this.createDateTime = date;
    }

    public Long getNominal() {
      return nominal;
    }

    public void setNominal(Long nominal) {
      this.nominal = nominal;
    }
}
