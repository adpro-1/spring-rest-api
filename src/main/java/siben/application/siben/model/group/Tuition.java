package siben.application.siben.model.group;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="tuitions")
public class Tuition {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "group_tuition",
      joinColumns =
        { @JoinColumn(name = "tuition_id", referencedColumnName = "id") },
      inverseJoinColumns =
        { @JoinColumn(name = "group_id", referencedColumnName = "id") })
    private Group group;

    @OneToMany(mappedBy = "tuition", cascade = CascadeType.ALL)
    private Set<MemberTuition> memberTuitions;

    @Column(name="nominal")
    private Long nominal;

    @Column(name="is_active")
    private boolean isActive = true;

    @Column(name="title")
    private String title;

    @Column(name="is_notified")
    private boolean isNotified = false;

    public Tuition() {

    }

    public Tuition(String title, Long nominal, Group group) {
        this.setTitle(title);
        this.setNominal(nominal);
        this.setGroup(group);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNominal(){
        return this.nominal;
    }

    public void setNominal(Long nominal){
        this.nominal = nominal;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Group getGroup() {
        return this.group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setMemberTuition(Set<MemberTuition> memberTuitions) {
        this.memberTuitions = memberTuitions;
    }

    public Set<MemberTuition> getMemberTuitions() {
        return this.memberTuitions;
    }

    public void setIsNotified(boolean isNotified){
        this.isNotified = isNotified;
    }

    public boolean getIsNotified(){
        return this.isNotified;
    }
}
