package siben.application.siben.model.group;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.CreationTimestamp;

import siben.application.siben.model.user.User;

@Entity
@Table(name="member_tuitions")
public class MemberTuition {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "tuition",
      joinColumns =
        { @JoinColumn(name = "member_tuition_id", referencedColumnName = "id") },
      inverseJoinColumns =
        { @JoinColumn(name = "tuition_id", referencedColumnName = "id") })
    private Tuition tuition;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "member_group_tuition",
      joinColumns =
        { @JoinColumn(name = "member_tuition_id", referencedColumnName = "id") },
      inverseJoinColumns =
        { @JoinColumn(name = "user_id", referencedColumnName = "id") })
    private User member;

    @Column(name="is_paid")
    private boolean isPaid=false;

    @Column(name="is_verified")
    private boolean isVerified=false;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    public MemberTuition() {

    }

    public MemberTuition(Tuition tuition, User member){
        this.setMember(member);
        this.setTuition(tuition);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDateTime() {
        return this.createDateTime;
    }

    public void setCreatedDateTime(LocalDateTime date) {
        this.createDateTime = date;
    }

    public boolean getIsPaid() {
        return this.isPaid;
    }

    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    public boolean getIsVerified() {
        return this.isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public void setTuition(Tuition tuition) {
        this.tuition = tuition;
    }

    public Tuition getTuition(){
        return this.tuition;
    }

    public void setMember(User member){
        this.member = member;
    }

    public User getMember(){
        return this.member;
    }
}
