package siben.application.siben.model.group.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import org.springframework.boot.jackson.JsonComponent;
import siben.application.siben.model.group.RequestBudget;

@JsonComponent
public class BudgetRequestSerializer {
    public static class Serialize extends JsonSerializer<RequestBudget> {
        @Override
        public void serialize(RequestBudget request, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeStartObject();
            jgen.writeNumberField("id", request.getId());
            jgen.writeStringField("timestamp", request.getCreatedDateTime().toString());
            jgen.writeNumberField("status", request.getStatus());
            jgen.writeNumberField("amount", request.getNominal());
            jgen.writeObjectField("user", request.getUser());
            jgen.writeStringField("group",request.getGroup().getName());
            jgen.writeEndObject();
        }
    }
}
