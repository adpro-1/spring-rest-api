package siben.application.siben.model.group.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import org.springframework.boot.jackson.JsonComponent;

import siben.application.siben.model.group.MemberTuition;

@JsonComponent
public class MemberTuitionSerializer {
    public static class Serialize extends JsonSerializer<MemberTuition> {
        @Override
        public void serialize(MemberTuition request, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeStartObject();
            jgen.writeNumberField("id", request.getId());
            jgen.writeStringField("timestamp", request.getCreatedDateTime().toString());
            jgen.writeStringField("title", request.getTuition().getTitle());
            jgen.writeStringField("group", request.getTuition().getGroup().getName());
            jgen.writeNumberField("nominal", request.getTuition().getNominal());
            jgen.writeBooleanField("isPaid", request.getIsPaid());
            jgen.writeBooleanField("isVerified", request.getIsVerified());
            jgen.writeStringField("user", request.getMember().getUsername());
            jgen.writeEndObject();
        }
    }
}
