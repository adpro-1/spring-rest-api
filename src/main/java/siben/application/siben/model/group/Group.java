package siben.application.siben.model.group;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import siben.application.siben.model.user.User;

@Entity
@Table(name = "groups")
public class Group implements Serializable {
    private final int nameColumnLength = 150;
    private final int codeColumnLength = 50;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="name", length = nameColumnLength, nullable = false, unique=true)
    private String name;

    @Column(name="code", length = codeColumnLength, nullable=false, unique=true)
    private String code;

    @Column(name="budget", nullable = false)
    private BigInteger budget = BigInteger.ZERO;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "group_creator",
      joinColumns =
        { @JoinColumn(name = "group_id", referencedColumnName = "id") },
      inverseJoinColumns =
        { @JoinColumn(name = "user_id", referencedColumnName = "id") })
    private User creator;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "group_members",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> members = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "group_admins",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> admins = new HashSet<>();

   @JsonIgnore
   @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
   private Set<MembershipRequest> membershipRequest = new HashSet<>();

   @JsonIgnore
   @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
   private Set<RequestBudget> requestBudget = new HashSet<>();

   @JsonIgnore
   @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
   private Set<Tuition> tuitions = new HashSet<>();

    public Group(String name, String code, BigInteger budget) {
        this.name = name;
        this.code = code;
        this.budget = budget;
    }

    public Group(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Set<User> getAdmins() {
        return admins;
    }

    public void setAdmins(Set<User> admins) {
        this.admins = admins;
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    public void setBudget(BigInteger newBudget) {
        this.budget = newBudget;
    }

    public BigInteger getBudget() {
        return budget;
    }

    public Set<MembershipRequest> getMembershipRequest() {
        Set<MembershipRequest> request = new HashSet<>();
        for (MembershipRequest r : this.membershipRequest) {
            if (r.getStatus() == 0) {
                request.add(r);
            }
        }
        return request;
    }

    public void setMembershipRequest(Set<MembershipRequest> request) {
        this.membershipRequest = request;
    }

    public void setRequestBudget(Set<RequestBudget> requestBudget) {
        this.requestBudget = requestBudget;
    }

    public Set<RequestBudget> getRequestBudget() {
        Set<RequestBudget> request = new HashSet<>();
        for (RequestBudget r : this.requestBudget) {
            if (r.getStatus() == 0) {
                request.add(r);
            }
        }
        return request;
    }

    public void setTuition(Set<Tuition> tuitions) {
        this.tuitions = tuitions;
    }

    public Set<Tuition> getTuition() {
        return this.tuitions;
    }
}
