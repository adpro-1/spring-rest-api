package siben.application.siben.group.payloads;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CreateMembershipRequestTest {
    private CreateMembershipRequest request;

    @BeforeEach
    public void setUp() throws Exception {
        this.request = new CreateMembershipRequest("test", "test12345");
    }

    @Test
    public void testConstructor() {
        CreateMembershipRequest blank = new CreateMembershipRequest();
        assertTrue(blank instanceof CreateMembershipRequest);
    }

    @Test
    public void testGetMethod() {
        assertEquals("test", request.getUsername());
        assertEquals("test12345", request.getCode());
    }
}
