package siben.application.siben.group.payloads;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateTuitionRequestTest {
    private CreateTuitionRequest request;

    @BeforeEach
    public void setUp() {
        request = new CreateTuitionRequest("jonny", "test", Long.valueOf(1), Long.valueOf(1));
    }

    @Test
    public void testBlankConstructor() {
        CreateTuitionRequest blank = new CreateTuitionRequest();
        assertTrue(blank instanceof CreateTuitionRequest);
    }

    @Test
    public void testGetMethod() {
        assertEquals("jonny", request.getUsername());
        assertEquals("test", request.getTitle());
        assertEquals(Long.valueOf(1), request.getGroupId());
        assertEquals(Long.valueOf(1), request.getNominal());
    }
}
