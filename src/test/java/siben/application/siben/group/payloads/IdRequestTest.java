package siben.application.siben.group.payloads;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IdRequestTest {
    private IdRequest request1;
    private IdRequest request2;
    private Long id = (long) 1;
    private String username = "test";
    private int status = 0;


    @BeforeEach
    public void setUp(){
        request1 = new IdRequest(id, username);
        request2 = new IdRequest(id, username, status);
    }


    @Test
    public void testConstructor(){
        IdRequest blank = new IdRequest();
        assertTrue(blank instanceof IdRequest);
    }

    @Test
    public void testGetMethod(){
        assertEquals((long)1, request1.getId());
        assertEquals("test", request1.getUsername());
        assertEquals(0, request2.getStatus());
    }
}
