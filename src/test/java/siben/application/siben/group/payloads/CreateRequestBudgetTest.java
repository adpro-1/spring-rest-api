package siben.application.siben.group.payloads;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

public class CreateRequestBudgetTest {
    private String uniqueCode = "UNIQUE";
    private String tester = "TESTER";
    private final int oneHundred = 100;
    private String amount = "100";
    private CreateRequestBudget createRequestBudget;

    @BeforeEach
    public void setUp() {
        createRequestBudget = new CreateRequestBudget(tester, uniqueCode, amount);
    }

    @Test
    public void testBlankConstructor() {
        CreateRequestBudget newReq = new CreateRequestBudget();
    }

    @Test
    public void testGetUsername() {
        assertEquals(tester, createRequestBudget.getUsername());
    }

    @Test
    public void testGetCode() {
        assertEquals(uniqueCode, createRequestBudget.getCode());
    }

    @Test
    public void testGetAmount() {
        Long longAmount = Long.parseLong(amount);
        assertEquals(longAmount, createRequestBudget.getAmount());
    }

    @Test
    public void testSetUsername() {
        String newUser = "NEWGUY";
        createRequestBudget.setUsername(newUser);
        assertEquals(newUser, createRequestBudget.getUsername());
    }

    @Test
    public void testSetCode() {
        String newCode = "NEWKEY";
        createRequestBudget.setCode(newCode);
        assertEquals(newCode, createRequestBudget.getCode());
    }

    @Test
    public void testSetAmount() {
        String newAmount = "101";
        Long amount = Long.parseLong(newAmount);
        createRequestBudget.setAmount(newAmount);
        assertEquals(amount, createRequestBudget.getAmount());
    }

}
