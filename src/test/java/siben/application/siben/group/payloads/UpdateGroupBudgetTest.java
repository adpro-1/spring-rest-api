package siben.application.siben.group.payloads;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UpdateGroupBudgetTest {

    private UpdateGroupBudget updateGroupBudget;

    @BeforeEach
    public void setUp() throws Exception {
        this.updateGroupBudget = new UpdateGroupBudget(BigInteger.valueOf(0), "test");
    }

    @Test
    public void testConstructor() {
        UpdateGroupBudget blank = new UpdateGroupBudget();
    }

    @Test
    public void testGetMethod() {
       assertEquals("test", updateGroupBudget.getUsername());
       assertEquals(BigInteger.valueOf(0), updateGroupBudget.getBudget());
    }

    @Test
    public void testSetMethod() {
        updateGroupBudget.setBudget(BigInteger.valueOf(1));
        updateGroupBudget.setUsername("halo");
        assertEquals(BigInteger.valueOf(1), updateGroupBudget.getBudget());
        assertEquals("halo", updateGroupBudget.getUsername());
    }
}
