package siben.application.siben.group.payloads;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;

public class CreateGroupRequestTest {

    private CreateGroupRequest request;
    private final int minSize = 4;
    private final int maxSize = 40;
    private final int range = 20;

    @BeforeEach
    public void setUp() throws Exception {
        this.request = new CreateGroupRequest("test", BigInteger.valueOf(0), "test");
    }

    @Test
    public void testConstructor() {
        CreateGroupRequest blank = new CreateGroupRequest();
    }

    @Test
    public void testGetMethod() {
       assertEquals("test", request.getName());
       assertEquals(BigInteger.valueOf(0), request.getBudget());
       assertEquals("test", request.getUsername());
       assertTrue(request.getUniqueCode() instanceof String);
    }

    @Test
    public void testSetMethod() {
        request.setBudget(BigInteger.valueOf(1));
        request.setName("halo");
        request.setUsername("halo");
        assertEquals("halo", request.getName());
        assertEquals(BigInteger.valueOf(1), request.getBudget());
        assertEquals("halo", request.getUsername());
    }

}
