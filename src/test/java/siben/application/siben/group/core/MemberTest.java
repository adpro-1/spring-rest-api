package siben.application.siben.group.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import siben.application.siben.model.user.User;

public class MemberTest {
    private Member member;
    private User user;

    @BeforeEach
    public void setUp() {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        user = new User(name, username, email, password);
        member = new Member(user);
    }

    @Test
    public void testGetMethod(){
        assertEquals("Jonny", member.getName());
        assertEquals("JonnyDepth", member.getUsername());
        assertEquals("jonny@noano.tech", member.getEmail());
        assertEquals("JonnyDepth", member.toString());
    }
}
