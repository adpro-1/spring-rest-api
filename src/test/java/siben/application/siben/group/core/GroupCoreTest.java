package siben.application.siben.group.core;

import java.math.BigInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.user.User;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GroupCoreTest {
    private GroupCore gc;
    private Group group;
    private User user;
    private final int budgetNominal = 10000;

    @BeforeEach
    public void setUp(){
        String code = "test123435";
        String groupName = "test";
        BigInteger budget =  BigInteger.valueOf(budgetNominal);
        group = new Group(groupName, code, budget);
        gc = new GroupCore(group);
        user = new User();
    }

    @Test
    public void testSetGet(){
        Anggota a = new Member(user);
        Anggota b = new Admin(user);
        gc.addMember(a);
        gc.addAdmins(b);
        assertEquals(1, gc.getAllMember().size());
        assertEquals(1, gc.getAllAdmin().size());
        assertEquals(this.group, this.gc.getGroup());
    }
}
