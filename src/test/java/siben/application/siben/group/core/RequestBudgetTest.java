package siben.application.siben.group.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.RequestBudget;
import siben.application.siben.model.user.User;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigInteger;

public class RequestBudgetTest {
    private RequestBudgetCore testRequest;
    private final int testNumber = 100;
    private final int statusNumber = 0;
    private Group group = new Group("Test", "test", BigInteger.valueOf(testNumber));
    private User user = new User("Test", "test", "test", "test");
    private RequestBudget requestBudget = new RequestBudget(user, group, statusNumber, (long) testNumber);

    @BeforeEach
    public void setUp() {
        testRequest = new RequestBudgetCore(requestBudget, group, user);
    }

    @Test
    public void testGetGroup() {
        assertEquals(group, testRequest.getGroup());
    }

    @Test
    public void testGetUser() {
        assertEquals(user, testRequest.getUser());
    }

    @Test
    public void testGetRequestBudget() {
        assertEquals(requestBudget, testRequest.getRequestBudget());
    }

    @Test
    public void testSetGroup() {
        Group newGroup = new Group("Test", "test", BigInteger.valueOf(testNumber));
        testRequest.setGroup(newGroup);
        assertEquals(newGroup, testRequest.getGroup());
    }

    @Test
    public void testSetUser() {
        User newUser = new User("Test", "test", "test", "test");
        testRequest.setUser(newUser);
        assertEquals(newUser, testRequest.getUser());
    }

    @Test
    public void testSetRequestBudget() {
        Group newGroup = new Group("Test", "test", BigInteger.valueOf(testNumber));
        User newUser = new User("Test", "test", "test", "test");
        RequestBudget newRequestBudget = new RequestBudget(newUser, newGroup, statusNumber, (long) testNumber);
        testRequest.setRequestBudget(newRequestBudget);
        assertEquals(newRequestBudget, testRequest.getRequestBudget());
    }

}
