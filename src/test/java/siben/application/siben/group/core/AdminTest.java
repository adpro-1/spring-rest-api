package siben.application.siben.group.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import siben.application.siben.model.user.User;

public class AdminTest {
    private Admin admin;
    private User user;

    @BeforeEach
    public void setUp() {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        user = new User(name, username, email, password);
        admin = new Admin(user);
    }

    @Test
    public void testGetUser() {
        assertEquals(user, admin.getUser());
    }

    @Test
    public void testGetMethod(){
        assertEquals("Jonny", admin.getName());
        assertEquals("JonnyDepth", admin.getUsername());
        assertEquals("jonny@noano.tech", admin.getEmail());
        assertEquals("JonnyDepth", admin.toString());
    }

    @Test
    public void testCompareTo(){
        user.setId(Long.valueOf(1));
        User other = new User();
        other.setId(Long.valueOf(1));
        Admin otherAdmin = new Admin(other);
        assertEquals(0, admin.compareTo(otherAdmin));
        other.setId(Long.valueOf(0));
        assertEquals(-1, admin.compareTo(otherAdmin));
    }

}

