package siben.application.siben.group.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import siben.application.siben.model.user.User;

public class AnggotaTest {

    private User a;
    private User b;
    private Anggota c;
    private Anggota d;


    @BeforeEach
    public void setUp(){
        a = new User();
        b = new User();
        a.setId(Long.valueOf(1));
        b.setId(Long.valueOf(1));
        c = new Member(a);
        d = new Member(b);
    }

    @Test
    public void testCompareTo(){
        assertEquals(0, c.compareTo(d));
        b.setId(Long.valueOf(0));
        assertEquals(-1, c.compareTo(d));
    }
}
