package siben.application.siben.group.service;

import java.io.IOException;
import java.math.BigInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import siben.application.siben.group.core.Member;
import siben.application.siben.group.repository.GroupRepository;
import siben.application.siben.group.repository.TuitionRepository;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class AdminGroupServiceTest {

    @InjectMocks
    private AdminGroupServiceImpl service;

    @MockBean
    private GroupRepository groupRepository;

    @MockBean
    private TuitionRepository tuitionRepository;

    @MockBean
    private MailService mailService;

    private User user;
    private Group group;
    private Tuition tuition;
    private final int budgetNominal = 1000;

    @BeforeEach
    public void setUp() {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        String code = "test123435";
        String groupName = "test";
        BigInteger budget =  BigInteger.valueOf(budgetNominal);
        user = new User(name, username, email, password);
        group = new Group(groupName, code, budget);
        tuition = new Tuition("test", Long.valueOf(1), group);
    }

    @Test
    public void testCreateGroup() {
        service.createGroup(user, group);
        service.editBudget(group, BigInteger.valueOf(1));
    }

    @Test
    public void testGetMethod() {
        service.getAllGroup(user);
        service.getListUser(group);
    }

    @Test
    public void testDelete() {
        service.deleteGroup(group);
    }

    @Test
    public void testAddRemoveUser() {
        service.addUser(new Member(user), group);
        service.addAdmin(new Member(user), group);
        service.removeUser(new Member(user), group);
    }

    @Test
    public void testTuition() throws IOException {
        service.addUser(new Member(user), group);
        service.addAdmin(new Member(user), group);
        service.addTuition(tuition, group);
        service.notifyTuition(tuition, group);
    }

}
