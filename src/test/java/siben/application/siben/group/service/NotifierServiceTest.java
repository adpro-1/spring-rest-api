package siben.application.siben.group.service;

import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashSet;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.springframework.boot.test.mock.mockito.MockBean;


import siben.application.siben.group.repository.RequestBudgetRepository;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;

import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class NotifierServiceTest {

    @InjectMocks
    private NotifierServiceImpl service;

    @MockBean
    private MailServiceImpl mailService;

    @MockBean
    private RequestBudgetRepository repo;

    private User user;
    private Group group;
    private Tuition tuition;
    private MemberTuition mt;
    private final int budgetNominal = 1000;

    @BeforeEach
    public void setUp() {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        String code = "test123435";
        String groupName = "test";
        BigInteger budget =  BigInteger.valueOf(budgetNominal);
        user = new User(name, username, email, password);
        group = new Group(groupName, code, budget);
        tuition = new Tuition("test", Long.valueOf(1), group);
        mt = new MemberTuition(tuition, user);
        Set<User> dummy = new HashSet<>();
        dummy.add(user);
        group.setAdmins(dummy);
        group.setMembers(dummy);
        Set<MemberTuition> mtDummy = new HashSet<>();
        mtDummy.add(mt);
        tuition.setMemberTuition(mtDummy);
    }

    @Test
    public void testRequestBudget() throws IOException {
        service.requestBudget(user, group, Long.valueOf(budgetNominal));
    }

    @Test
    public void testRequestMembership() throws IOException {
        service.requestMembership(user, group);
    }

    @Test
    public void testReportPayment() throws IOException {
        service.reportPayment(mt);
    }

    @Test
    public void testVerifPayment() throws IOException {
        service.reportVerifPayment(mt);
    }

    @Test
    public void testGetAllRequestBudget() {
        service.getAllRequestBudget();
    }

    @Test
    public void testGetRequestBudgtet() {
        service.getRequestById(Long.valueOf(1));
    }

}
