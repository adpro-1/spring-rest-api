package siben.application.siben.group.service;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;

@ExtendWith(MockitoExtension.class)
public class MailServiceTest {
    private User user;
    private Group group;
    private Tuition tuition;
    private MemberTuition mt;
    private final int budgetNominal = 1000;

    @InjectMocks
    private MailServiceImpl mailService;

    @BeforeEach
    public void setUp() {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        String code = "test123435";
        String groupName = "test";
        BigInteger budget =  BigInteger.valueOf(budgetNominal);
        user = new User(name, username, email, password);
        group = new Group(groupName, code, budget);
        tuition = new Tuition("test", Long.valueOf(1), group);
        mt = new MemberTuition(tuition, user);
        Set<User> dummy = new HashSet<>();
        dummy.add(user);
        group.setAdmins(dummy);
        group.setMembers(dummy);
        Set<MemberTuition> mtDummy = new HashSet<>();
        mtDummy.add(mt);
        tuition.setMemberTuition(mtDummy);
    }

    @Test
    public void testRequestBudget() throws IOException {
        mailService.sendRequestBudget(user, group, Long.valueOf(budgetNominal));
    }

    @Test
    public void testRequestMember() throws IOException {
        mailService.sendRequestMember(user, group);
    }

    @Test
    public void testReportPayTuition() throws IOException {
        mailService.sendReportPayTuition(mt);
    }

    @Test
    public void testReportVerifyTuition() throws IOException {
        mailService.sendReportVerifyTuition(mt);
    }

    @Test
    public void testSendTuition() throws IOException {
        mailService.sendTuition(tuition);
    }
}
