package siben.application.siben.group.service;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import siben.application.siben.group.core.Admin;
import siben.application.siben.group.core.Anggota;
import siben.application.siben.group.repository.GroupRepository;
import siben.application.siben.model.group.Group;
import siben.application.siben.model.user.User;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class GroupServiceTest {

    @InjectMocks
    private GroupServiceImpl service;

    @MockBean
    private GroupRepository repo;

    private User user;
    private User admin;
    private Group group;
    private final int budgetNominal = 1000;

    @BeforeEach
    public void setUp() {
        String name = "Jonny";
        String username = "JonnyDepth";
        String username1 = "Hahaha";
        String email1 = "haha@haha.com";
        String email = "jonny@noano.tech";
        String password = "test12345";
        String code = "test123435";
        String groupName = "test";
        BigInteger budget =  BigInteger.valueOf(budgetNominal);
        user = new User(name, username, email, password);
        group = new Group(groupName, code, budget);
        Set<User> dummy = new HashSet<>();
        Set<User> dummyAdmin = new HashSet<>();
        admin = new User(name, username1, email1, password);
        user.setId(Long.valueOf(1));
        admin.setId(Long.valueOf(2));
        dummy.add(user);
        dummyAdmin.add(admin);
        dummy.add(admin);
        group.setAdmins(dummyAdmin);
        group.setMembers(dummy);
    }

    @Test
    public void testSetUp() {
        service.setUp(group);
        service.getAllMember();
        service.getAllAdmin();
        service.getGroup();
        service.getMembershipRequest(group);
        service.getRequestBudget(group);
        Anggota test = new Admin(user);
        Anggota test2 = new Admin(admin);
        User blank = new User();
        blank.setId(Long.valueOf(0));
        Anggota unknown = new Admin(blank);
        service.cekIsAdmin(test);
        service.cekIsAdmin(test2);
        service.cekIsMember(test);
        service.cekIsMember(unknown);
    }
}
