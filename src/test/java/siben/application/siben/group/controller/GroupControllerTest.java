package siben.application.siben.group.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import siben.application.siben.group.repository.GroupRepository;
import siben.application.siben.group.repository.MemberTuitionRepository;
import siben.application.siben.group.repository.MembershipRequestRepository;
import siben.application.siben.group.repository.RequestBudgetRepository;
import siben.application.siben.group.repository.TuitionRepository;
import siben.application.siben.group.service.AdminGroupService;
import siben.application.siben.group.service.GroupService;
import siben.application.siben.group.service.NotifierService;
import siben.application.siben.model.user.User;
import siben.application.siben.user.repository.RoleRepository;
import siben.application.siben.user.repository.UserRepository;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = GroupController.class)

public class GroupControllerTest {

    private User mockUser;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RequestBudgetRepository requestBudgetRepository;

    @MockBean
    private NotifierService notifierService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private GroupRepository groupRepository;

    @MockBean
    private AdminGroupService adminGroupService;

    @MockBean
    private GroupService groupService;

    @MockBean
    private TuitionRepository tuitionRepository;

    @MockBean
    private MemberTuitionRepository memberTuitionRepository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private MembershipRequestRepository membershipRequestRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    // @Test
    public void testGetUserGroup() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/group/{username}", "master"));
        when(userRepository.findByUsername("master").get()).thenReturn(this.mockUser);
        verify(userRepository, times(1)).findByUsername("master");
    }
}
