package siben.application.siben.model.group;

import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import siben.application.siben.model.user.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MembershipRequestTest {

    private User mockUser;
    private Group mockGroup;

    @BeforeEach
    public void setUp() throws Exception {
        this.mockUser = new User();
        this.mockGroup = new Group();
    }

    @Test
    public void testBlankConstructor() {
        MembershipRequest blank = new MembershipRequest();
        assertTrue(blank instanceof MembershipRequest);
    }

    @Test
    public void testConstructor() {
        MembershipRequest request = new MembershipRequest(mockUser, mockGroup, 0);
        assertTrue(request instanceof MembershipRequest);
        assertEquals(mockUser, request.getUser());
        assertEquals(mockGroup, request.getGroup());
        assertEquals(0, request.getStatus());
    }

    @Test
    public void testSetterGetter() {
        MembershipRequest request = new MembershipRequest(mockUser, mockGroup, 0);
        request.setId(Long.valueOf(1));
        request.setCreatedDateTime(LocalDateTime.now());
        assertEquals(1, request.getId());
        assertTrue(request.getCreatedDateTime() instanceof LocalDateTime);
    }

    @Test
    public void testErrorSetStatus() {
        final int status = 10;
        MembershipRequest request = new MembershipRequest(mockUser, mockGroup, 0);
        Assertions.assertThrows(NumberFormatException.class, () -> request.setStatus(status));
        Assertions.assertThrows(NumberFormatException.class, () -> request.setStatus(-1 * status));
    }

}
