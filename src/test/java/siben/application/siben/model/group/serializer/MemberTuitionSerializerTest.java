package siben.application.siben.model.group.serializer;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.Tuition;
import siben.application.siben.model.user.User;

@ExtendWith(SpringExtension.class)
@AutoConfigureDataJpa
@JsonTest
public class MemberTuitionSerializerTest {
    @Autowired
    private JacksonTester<MemberTuition> json;
    private MemberTuition mockMt;
    private User mockUser;
    private Tuition mockTuition;
    private Group mockGroup;
    private final int oneHundred = 100;

    @BeforeEach
    public void setUp() {
        int testNumber = oneHundred;
        this.mockGroup = new Group("Test", "Test", BigInteger.valueOf(testNumber));
        this.mockTuition = new Tuition("test", Long.valueOf(1), this.mockGroup);
        this.mockUser = new User();
        this.mockMt = new MemberTuition(mockTuition, mockUser);
        this.mockMt.setId(Long.valueOf(1));
        this.mockMt.setCreatedDateTime(LocalDateTime.now());
    }

    @Test
    public void testSerializer() throws Exception {
        JsonContent<MemberTuition> result = this.json.write(this.mockMt);
        assertThat(result).hasJsonPathStringValue("$.title");
    }
}
