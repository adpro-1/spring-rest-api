package siben.application.siben.model.group.serializer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import java.time.LocalDateTime;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.RequestBudget;
import siben.application.siben.model.user.User;



@ExtendWith(SpringExtension.class)
@AutoConfigureDataJpa
@JsonTest
public class BudgetRequestSerializerTest {
    @Autowired
    private JacksonTester<RequestBudget> json;
    private final int testNumber = 100;
    private User user = new User("Test", "test", "test@test.com", "test");
    private Group group = new Group("Test", "Test", BigInteger.valueOf(testNumber));
    private RequestBudget requestBudget;

    @Test
    public void testSerializer() throws Exception {
        requestBudget = new RequestBudget(user, group, 0, (long) testNumber);
        requestBudget.setId(Long.valueOf(1));
        LocalDateTime time = LocalDateTime.now();
        requestBudget.setCreatedDateTime(time);
        JsonContent<RequestBudget> result = this.json.write(requestBudget);
        assertThat(result).extractingJsonPathNumberValue("$.status").hasToString("0");
    }
}
