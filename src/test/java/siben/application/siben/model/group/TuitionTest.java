package siben.application.siben.model.group;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TuitionTest {

    private Group mockGroup;
    private Tuition mockTuition;

    @BeforeEach
    public void setUp(){
        this.mockGroup = new Group();
        this.mockTuition = new Tuition("test", Long.valueOf(1), this.mockGroup);
    }

    @Test
    public void testBlankContructor() {
        Tuition blankTuition = new Tuition();
        assertTrue(blankTuition instanceof Tuition);
    }

    @Test
    public void testGetId() {
        mockTuition.setId(Long.valueOf(1));
        assertEquals(Long.valueOf(1), mockTuition.getId());
    }

    @Test
    public void testGetTitle(){
        assertEquals("test", this.mockTuition.getTitle());
    }

    @Test
    public void testIsNotified() {
        this.mockTuition.setIsNotified(true);
        assertTrue(this.mockTuition.getIsNotified());
    }

    @Test
    public void testGetNominal() {
        assertEquals(Long.valueOf(1), this.mockTuition.getNominal());
    }

    @Test
    public void testGetGroup() {
        assertEquals(this.mockGroup, this.mockTuition.getGroup());
    }

    @Test
    public void testMemberTuition(){
        MemberTuition mt = new MemberTuition();
        Set<MemberTuition> temp = new HashSet<>();
        temp.add(mt);
        this.mockTuition.setMemberTuition(temp);
        assertTrue(this.mockTuition.getMemberTuitions().size() == 1);
    }
}
