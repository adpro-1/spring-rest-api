package siben.application.siben.model.group;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import siben.application.siben.model.user.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class GroupTest {

    private User mockUser;
    private Group mockGroup;
    private final int budgetNominal = 1000;

    @BeforeEach
    public void setUp() throws Exception {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        String code = "test123435";
        String groupName = "test";
        BigInteger budget =  BigInteger.valueOf(budgetNominal);
        this.mockUser = new User(name, username, email, password);
        this.mockGroup = new Group(groupName, code, budget);
    }

    @Test
    public void testConstructor() {
        Group blankGroup = new Group();
        assertTrue(blankGroup instanceof Group);
    }

    @Test
    public void testSetMethod() {
        Set<User> mockAdmin = new HashSet<User>();
        mockAdmin.add(mockUser);
        this.mockGroup.setAdmins(mockAdmin);
        this.mockGroup.setCreator(mockUser);
        this.mockGroup.setMembers(mockAdmin);
        this.mockGroup.setId(Long.valueOf(1));
        this.mockGroup.setBudget(BigInteger.valueOf(0));
        this.mockGroup.setName("Halo");
        this.mockGroup.setCode("Halo12345");

        assertEquals(1, this.mockGroup.getAdmins().size());
        assertEquals(1, this.mockGroup.getMembers().size());
        assertTrue(this.mockGroup.getCreator() instanceof User);
        assertEquals(1, this.mockGroup.getId());
    }

    @Test
    public void testGetMethod() {
        assertEquals("test", this.mockGroup.getName());
        assertEquals("test123435", this.mockGroup.getCode());
        assertEquals(BigInteger.valueOf(budgetNominal), this.mockGroup.getBudget());
    }

    @Test
    public void testMembership() {
        MembershipRequest request = new MembershipRequest();
        Set<MembershipRequest> list = new HashSet<MembershipRequest>();
        list.add(request);
        this.mockGroup.setMembershipRequest(list);
        assertEquals(1, this.mockGroup.getMembershipRequest().size());
    }

    @Test
    public void testSetRequestBudget() {
        Set<RequestBudget> newRequest = new HashSet<>();
        mockGroup.setRequestBudget(newRequest);
        assertEquals(newRequest, mockGroup.getRequestBudget());
    }

    @Test
    public void testGetRequestBudget() {
        Set<RequestBudget> newRequest = new HashSet<>();
        mockGroup.setRequestBudget(newRequest);
        assertEquals(newRequest, mockGroup.getRequestBudget());
        Set<RequestBudget> newRequest2 = new HashSet<>();
        RequestBudget nr = new RequestBudget();
        nr.setStatus(0);
        newRequest2.add(nr);
        mockGroup.setRequestBudget(newRequest2);
        assertTrue(mockGroup.getRequestBudget().size() == 1);
    }

    @Test
    public void testTuition(){
        Set<Tuition> t = new HashSet<>();
        Tuition nt = new Tuition();
        t.add(nt);
        this.mockGroup.setTuition(t);
        assertTrue(this.mockGroup.getTuition().size() == 1);
    }
}
