package siben.application.siben.model.group;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import siben.application.siben.model.user.User;

public class MemberTuitionTest {
    private MemberTuition mockMt;
    private User mockUser;
    private Tuition mockTuition;


    @BeforeEach
    public void setUp() {
        this.mockUser = new User("Test", "test", "test@test.com", "test");
        this.mockMt = new MemberTuition(mockTuition, mockUser);
    }

    @Test
    public void testGetMethod(){
        LocalDateTime date = LocalDateTime.now();
        this.mockMt.setCreatedDateTime(date);
        this.mockMt.setId(Long.valueOf(1));
        this.mockMt.setIsPaid(true);
        this.mockMt.setIsVerified(true);
        assertEquals(date, this.mockMt.getCreatedDateTime());
        assertEquals(Long.valueOf(1), this.mockMt.getId());
        assertTrue(this.mockMt.getIsPaid());
        assertTrue(this.mockMt.getIsVerified());
        assertEquals(this.mockTuition, this.mockMt.getTuition());
        assertEquals(this.mockUser, this.mockMt.getMember());
    }
}
