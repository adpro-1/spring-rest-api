package siben.application.siben.model.group;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;
import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import siben.application.siben.model.user.User;

public class RequestBudgetTest {
    private final int testNumber = 100;
    private final int sixtyNine = 69;
    private User user = new User("Test", "test", "test@test.com", "test");
    private Group group = new Group("Test", "Test", BigInteger.valueOf(testNumber));
    private RequestBudget requestBudget;

    @BeforeEach
    public void setUp() {
        requestBudget = new RequestBudget(user, group, 0, (long) testNumber);
    }

    @Test
    public void testBlankConstructor() {
        RequestBudget newRequest = new RequestBudget();
    }

    @Test
    public void testSetId() {
        Long newId = (long) sixtyNine;
        requestBudget.setId(newId);
        assertEquals(newId, requestBudget.getId());
    }

    @Test
    public void testSetStatus() {
        int newStatus = 1;
        requestBudget.setStatus(newStatus);
        assertEquals(newStatus, requestBudget.getStatus());
    }

    @Test
    public void testSetStatusFail() {
        int newStatus = sixtyNine;
        assertThrows(NumberFormatException.class, () -> {
            requestBudget.setStatus(newStatus);
        });
    }

    @Test
    public void testSetGroup() {
        Group newGroup = new Group("Test", "Test", BigInteger.valueOf(testNumber));
        requestBudget.setGroup(newGroup);
        assertEquals(newGroup, requestBudget.getGroup());
    }

    @Test
    public void testSetUser() {
        User newUser = new User("Test", "test", "test@test.com", "test");
        requestBudget.setUser(newUser);
        assertEquals(newUser, requestBudget.getUser());
    }

    @Test
    public void testSetCreatedDateTime() {
        LocalDateTime currTime = LocalDateTime.now();
        requestBudget.setCreatedDateTime(currTime);
        assertEquals(currTime, requestBudget.getCreatedDateTime());
    }

    @Test
    public void testSetNominal() {
        Long newNominal = (long) sixtyNine;
        requestBudget.setNominal(newNominal);
        assertEquals(newNominal, requestBudget.getNominal());
    }
    @Test
    public void testGetId() {
        Long newId = (long) sixtyNine;
        requestBudget.setId(newId);
        assertEquals(newId, requestBudget.getId());
    }

    @Test
    public void testGetStatus() {
        int expectedStatus = 0;
        assertEquals(expectedStatus, requestBudget.getStatus());
    }

    @Test
    public void testGetGroup() {
        assertEquals(group, requestBudget.getGroup());
    }

    @Test
    public void testGetUser() {
        assertEquals(user, requestBudget.getUser());
    }

    @Test
    public void testGetCreatedDateTime() {
        LocalDateTime currTime = LocalDateTime.now();
        requestBudget.setCreatedDateTime(currTime);
        assertEquals(currTime, requestBudget.getCreatedDateTime());
    }

    @Test
    public void testGetNominal() {
        Long expectedNumber = (long) testNumber;
        assertEquals(expectedNumber, requestBudget.getNominal());
    }

}
