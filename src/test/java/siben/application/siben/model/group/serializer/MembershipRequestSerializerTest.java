package siben.application.siben.model.group.serializer;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MembershipRequest;
import siben.application.siben.model.user.User;

@ExtendWith(SpringExtension.class)
@AutoConfigureDataJpa
@JsonTest
public class MembershipRequestSerializerTest {
    @Autowired
    private JacksonTester<MembershipRequest> json;
    private User mockUser;
    private Group mockGroup;
    private final int oneHundred = 100;

    @BeforeEach
    public void setUp() throws Exception {
        int testNumber = oneHundred;
        this.mockUser = new User("Test", "test", "test@test.com", "test");
        this.mockGroup = new Group("Test", "Test", BigInteger.valueOf(testNumber));
    }

    @Test
    public void testSerializer() throws Exception {
        MembershipRequest request = new MembershipRequest(mockUser, mockGroup, 0);
        request.setId(Long.valueOf(1));
        request.setCreatedDateTime(LocalDateTime.now());
        JsonContent<MembershipRequest> result = this.json.write(request);
        assertThat(result).hasJsonPathStringValue("$.group");
    }
}
