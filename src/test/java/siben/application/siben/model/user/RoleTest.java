package siben.application.siben.model.user;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RoleTest {

    private Role mockRole;

    @BeforeEach
    public void setUp() throws Exception {
        this.mockRole = new Role(RoleName.ROLE_USER);
    }

    @Test
    public void testConstructor() {
        Role blankRole = new Role();
        assertTrue(blankRole instanceof Role);
    }

    @Test
    public void testGetName() {
        assertEquals(RoleName.ROLE_USER, this.mockRole.getName(), "Fail Test getName");
    }

    @Test
    public void testSetId() {
        this.mockRole.setId(Long.valueOf(1));
        assertEquals(1, this.mockRole.getId(), "Fail Test setId");
    }

    @Test
    public void testSetName() {
        this.mockRole.setName(RoleName.ROLE_ADMIN);
        assertNotEquals(RoleName.ROLE_USER, this.mockRole.getName(), "Fail Test setName");
    }
}
