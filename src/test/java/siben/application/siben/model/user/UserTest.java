package siben.application.siben.model.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import siben.application.siben.model.group.Group;
import siben.application.siben.model.group.MemberTuition;
import siben.application.siben.model.group.MembershipRequest;
import siben.application.siben.model.group.RequestBudget;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UserTest {
    private User mockUser;
    private Role mockRole;
    private final int oneHundred = 100;

    @BeforeEach
    public void setUp() throws Exception {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        this.mockUser = new User(name, username, email, password);
        this.mockRole = new Role(RoleName.ROLE_USER);
    }

    @Test
    public void testBlankConstructor() {
        User blankUser = new User();
        assertTrue(blankUser instanceof User);
    }

    @Test
    public void testRelation() {
        Set<Group> mockGroup = new HashSet<Group>();
        Group group = new Group();
        mockGroup.add(group);
        mockUser.setGroup(mockGroup);
        mockUser.setUserAdmin(mockGroup);
        assertEquals(1, this.mockUser.getUserAdmins().size());
        assertEquals(1, this.mockUser.getUserGroups().size());
    }

    @Test
    public void testGetName() {
        assertEquals("Jonny", this.mockUser.getName(), "Fail Test getName");
    }

    @Test
    public void testGetUsername() {
        assertEquals("JonnyDepth", this.mockUser.getUsername(), "Fauk Test getUsername");
    }

    @Test
    public void testGetEmail() {
        assertEquals("jonny@noano.tech", this.mockUser.getEmail(), "Fail Test getEmail");
    }

    @Test
    public void testGetPassword() {
        assertEquals("test12345", this.mockUser.getPassword(), "Fail Test getPassword");
    }

    @Test
    public void testGetId() {
        Long id = Long.valueOf(1);
        this.mockUser.setId(id);
        assertEquals(id, this.mockUser.getId(), "Fail Test getId");
    }

    @Test
    public void testSetName() {
        this.mockUser.setName("Tono");
        assertNotEquals("Jonny", this.mockUser.getName(), "Fail Test setName");
    }

    @Test
    public void testSetEmail() {
        this.mockUser.setEmail("tono@gmail.com");
        assertNotEquals("jonny@noano.tech", this.mockUser.getEmail(), "Fail Test setEmail");
    }

    @Test
    public void testSetUsername() {
        this.mockUser.setUsername("Tono");
        assertNotEquals("JonnyDepth", this.mockUser.getUsername(), "Fail Test setUsername");
    }

    @Test
    public void testSetPassword() {
        this.mockUser.setPassword("12345test");
        assertNotEquals("test12345", this.mockUser.getPassword(), "Fail Test setPassword");
    }

    @Test
    public void testSetRole() {
        this.mockUser.setRoles(Collections.singleton(this.mockRole));
        assertEquals(1, this.mockUser.getRoles().size(), "Fail Test setRole");
    }

    @Test
    public void testMembership() {
        MembershipRequest request = new MembershipRequest();
        Set<MembershipRequest> list = new HashSet<MembershipRequest>();
        list.add(request);
        this.mockUser.setMembershipRequest(list);
        assertEquals(1, this.mockUser.getMembershipRequest().size());
    }

    @Test
    public void testSetRequestBudget() {
        int testNumber = oneHundred;
        User newUser = new User("Test", "test", "test@test.com", "test");
        Group newGroup = new Group("Test", "Test", BigInteger.valueOf(testNumber));
        Long nominal = (long) oneHundred;
        RequestBudget newRequestBudget = new RequestBudget(newUser, newGroup, 0, nominal);
        Set<RequestBudget> newSet = new HashSet<>();
        newSet.add(newRequestBudget);
        mockUser.setRequestBudget(newSet, newGroup);
        assertEquals(newSet, mockUser.getRequestBudget());
    }

    @Test
    public void testGetRequestBudget() {
        int testNumber = oneHundred;
        User newUser = new User("Test", "test", "test@test.com", "test");
        Group newGroup = new Group("Test", "Test", BigInteger.valueOf(testNumber));
        Long nominal = (long) oneHundred;
        RequestBudget newRequestBudget = new RequestBudget(newUser, newGroup, 0, nominal);
        Set<RequestBudget> newSet = new HashSet<>();
        newSet.add(newRequestBudget);
        mockUser.setRequestBudget(newSet, newGroup);
        assertEquals(newSet, mockUser.getRequestBudget());
    }

    @Test
    public void testMemberTuition() {
        MemberTuition mt = new MemberTuition();
        Set<MemberTuition> smt = new HashSet<MemberTuition>();
        smt.add(mt);
        mockUser.setMemberTuition(smt);
        assertEquals(1, mockUser.getMemberTuition().size());
    }
}
