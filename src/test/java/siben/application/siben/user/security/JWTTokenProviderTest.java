package siben.application.siben.user.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import javax.naming.AuthenticationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import siben.application.siben.model.user.User;
import siben.application.siben.user.repository.UserRepository;

@ComponentScan("siben.application.siben.user.config")
@DataJpaTest
public class JWTTokenProviderTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomUserDetailsService cds;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtProvider;

    private Authentication authentication;
    private User mockUser;
    private String jwt;

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    @BeforeEach
    public void setUp() throws Exception {
        String name = "Jonny";
        String username = "JonnyDepth";
        String email = "jonny@noano.tech";
        String password = "test12345";
        this.mockUser = new User(name, username, email, password);
        this.mockUser.setPassword(passwordEncoder.encode(this.mockUser.getPassword()));
        this.mockUser.setId(Long.valueOf(1));
        userRepository.save(this.mockUser);
    }

    @Test
    public void testToken() throws AuthenticationException {
        User user = userRepository.findByUsername("JonnyDepth").get();
        this.authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), "test12345"));
        SecurityContextHolder.getContext().setAuthentication(this.authentication);
        this.jwt = jwtProvider.generateToken(this.authentication);
        assertTrue(jwtProvider.validateToken(jwt));
        assertTrue(!jwtProvider.validateToken(""));
        assertTrue(!jwtProvider.validateToken(this.createExpiredToken()));
        assertTrue(!jwtProvider.validateToken(this.createWrongSignatureToken()));
        assertTrue(!jwtProvider.validateToken("asdasd"));
        assertEquals(Long.valueOf(1), jwtProvider.getUserIdFromJWT(jwt));
        UserDetails userDetails = cds.loadUserById(user.getId());
        System.out.println(userDetails);
        try {
            userDetails = cds.loadUserById(Long.valueOf(2));
        } catch (Exception ex) {
            System.out.println(ex);
        }
        try {
            userDetails = cds.loadUserByUsername("HAHAHAH");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private String createExpiredToken() {
        UserSecurity userSecurity = (UserSecurity) authentication.getPrincipal();
        Date now = new Date();
        Date expiryDate = new Date(now.getTime());
        return Jwts.builder()
                .setSubject(Long.toString(userSecurity.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    private String createWrongSignatureToken() {
        UserSecurity userSecurity = (UserSecurity) authentication.getPrincipal();
        Date now = new Date();
        Date expiryDate = new Date(now.getTime());
        return Jwts.builder()
                .setSubject(Long.toString(userSecurity.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, "hahahah")
                .compact();
    }
}
