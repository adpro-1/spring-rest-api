package siben.application.siben.user.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import siben.application.siben.model.user.User;
import siben.application.siben.model.user.Role;
import siben.application.siben.model.user.RoleName;


public class UserSecurityTest {
    private User user;
    private Role userRole;
    private UserSecurity userSecurity;

    @BeforeEach
    public void setUp() {
        this.user = new User("test", "test", "test@test.com", "test");
        this.userRole = new Role(RoleName.ROLE_USER);
        this.user.setRoles(Collections.singleton(this.userRole));
        this.userSecurity = new UserSecurity(Long.valueOf(1), "test", "test", "test@test.com", "test", new ArrayList<GrantedAuthority>());
    }

    @Test
    public void testCreate() {
        UserSecurity test = this.userSecurity.create(this.user);
        assertTrue(test instanceof UserSecurity);
    }

    @Test
    public void testGetMethod() {
        SimpleGrantedAuthority test = new SimpleGrantedAuthority(user.getRoles().toString());
        assertTrue(this.userSecurity.isAccountNonExpired());
        assertTrue(this.userSecurity.isAccountNonLocked());
        assertTrue(this.userSecurity.isCredentialsNonExpired());
        assertTrue(this.userSecurity.isEnabled());
        assertTrue(test instanceof SimpleGrantedAuthority);
        assertTrue(this.userSecurity.hashCode() * 0 == 0);
        assertTrue(this.userSecurity.getAuthorities() instanceof Collection);
        assertTrue(this.userSecurity.equals(userSecurity));
        assertFalse(this.userSecurity.equals(user));
        assertFalse(this.userSecurity.equals(null));
        assertTrue(this.userSecurity.equals(new UserSecurity(Long.valueOf(1), "test", "test", "test@test.com", "test", new ArrayList<GrantedAuthority>())));
        assertEquals(Long.valueOf(1), this.userSecurity.getId(), "Fail getId");
        assertEquals("test", this.userSecurity.getName(), "Fail getName");
        assertEquals("test", this.userSecurity.getPassword(), "Fail getPassword");
        assertEquals("test", this.userSecurity.getUsername(), "Fail getUsername");
        assertEquals("test@test.com", this.userSecurity.getEmail(), "Fail getEmail");
    }
}
