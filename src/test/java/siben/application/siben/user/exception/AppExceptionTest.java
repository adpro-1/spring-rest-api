package siben.application.siben.user.exception;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AppExceptionTest {

    private AppException exception1;
    private AppException exception2;

    @BeforeEach
    public void setUp() throws Exception {
        this.exception1 = new AppException("error");
        this.exception2 = new AppException("error", new Throwable("error"));
    }

    @Test
    public void testInstance() {
        assertTrue(this.exception1 instanceof AppException);
        assertTrue(this.exception2 instanceof AppException);
    }
}
