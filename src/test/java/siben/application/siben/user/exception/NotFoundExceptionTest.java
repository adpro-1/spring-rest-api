package siben.application.siben.user.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NotFoundExceptionTest {
    private NotFoundException exception;

    @BeforeEach
    public void setUp() throws Exception {
        this.exception = new NotFoundException("test", "test", new Object());
    }

    @Test
    public void testGetMethod() {
        assertEquals("test", this.exception.getResourceName(), "Fail getResource");
        assertEquals("test", this.exception.getFieldName(), "Fail getFieldName");
        assertTrue(this.exception.getFieldValue() instanceof Object);
    }
}
