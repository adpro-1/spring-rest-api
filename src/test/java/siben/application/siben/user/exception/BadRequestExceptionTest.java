package siben.application.siben.user.exception;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BadRequestExceptionTest {
    private BadRequestException exception1;
    private BadRequestException exception2;

    @BeforeEach
    public void setUp() throws Exception {
        this.exception1 = new BadRequestException("error");
        this.exception2 = new BadRequestException("error", new Throwable("error"));
    }

    @Test
    public void testInstance() {
        assertTrue(this.exception1 instanceof BadRequestException);
        assertTrue(this.exception2 instanceof BadRequestException);
    }
}
