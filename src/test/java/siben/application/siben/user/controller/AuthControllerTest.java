package siben.application.siben.user.controller;

import org.springframework.http.MediaType;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import siben.application.siben.user.payloads.LoginRequest;
import siben.application.siben.user.payloads.RegisterRequest;
import siben.application.siben.user.repository.UserRepository;


@ExtendWith(SpringExtension.class)
@WebMvcTest(value = AuthController.class)
@AutoConfigureDataJpa
public class AuthControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    // @MockBean
    // private AuthenticationManager am;

    // @MockBean
    // private JwtTokenProvider token;

    // @MockBean
    // private RoleRepository roleRepository;

    @Autowired
    private ObjectMapper objMapper;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegistration() throws JsonProcessingException, Exception {
        RegisterRequest regis = new RegisterRequest("Jonny", "master", "test12345", "jonny@haha.tech");
        mvc.perform(MockMvcRequestBuilders.post("/api/auth/register").contentType(MediaType.APPLICATION_JSON)
                .content(objMapper.writeValueAsString(regis))).andReturn();
        Mockito.when(userRepository.existsByEmail(regis.getEmail())).thenReturn(true);
        verify(userRepository, times(1)).existsByEmail(regis.getEmail());
        Mockito.when(userRepository.existsByUsername(regis.getUsername())).thenReturn(true);
        verify(userRepository, times(1)).existsByUsername(regis.getUsername());

    }

    @Test
    public void testLogin() throws JsonProcessingException, Exception {
        LoginRequest request = new LoginRequest("jonny@haha.tech", "test12345");
        mvc.perform(MockMvcRequestBuilders.post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objMapper.writeValueAsString(request))).andReturn();
        // when(am.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword())))
        //                 .thenReturn(any(Authentication.class));
        // verify(am, times(1)).authenticate(any(Authentication.class));
    }
}
