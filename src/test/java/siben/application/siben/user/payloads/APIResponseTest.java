package siben.application.siben.user.payloads;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class APIResponseTest {

    private APIResponse response;

    @BeforeEach
    public void setUp() throws Exception {
        this.response = new APIResponse(true, "mock success");
    }

    @Test
    public void testGetMethod() {
        assertTrue(this.response.getSuccess());
        assertEquals("mock success", this.response.getMessage(), "Fail Test getMessage");
    }

    @Test
    public void testSetMethod() {
        this.response.setMessage("another message");
        this.response.setSuccess(false);
        assertNotEquals("mock message", this.response.getMessage(), "Fail Test setMessage");
        assertTrue(!this.response.getSuccess());
    }
}
