package siben.application.siben.user.payloads;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegisterRequestTest {

    private RegisterRequest request;

    @BeforeEach
    public void setUp() throws Exception {
        this.request = new RegisterRequest("test", "test", "test", "test@test.com");
    }

    @Test
    public void testConstructor() {
        RegisterRequest blankRegister = new RegisterRequest();
        assertTrue(blankRegister instanceof RegisterRequest);
    }

    @Test
    public void testGetMethod() {
        assertEquals("test", this.request.getName(), "Fail Test getName");
        assertEquals("test", this.request.getUsername(), "Fail Test getUsername");
        assertEquals("test", this.request.getPassword(), "Fail Test getPassword");
        assertEquals("test@test.com", this.request.getEmail(), "Fail Test getEmail");
    }

}
