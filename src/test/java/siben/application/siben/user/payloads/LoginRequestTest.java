package siben.application.siben.user.payloads;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginRequestTest {
    private LoginRequest request;

    @BeforeEach
    public void setUp() throws Exception {
        this.request = new LoginRequest("test@test.com", "test12345");
    }

    @Test
    public void testConstructor() {
        LoginRequest blankRequest = new LoginRequest();
        assertTrue(blankRequest instanceof LoginRequest);
    }

    @Test
    public void testGetEmail() {
        assertEquals("test@test.com", this.request.getEmail(), "Fail Test getEmail");
    }

    @Test
    public void testGetPassword() {
        assertEquals("test12345", this.request.getPassword(), "Fail Test getPassword");
    }

    @Test
    public void testSetAll() {
        this.request.setEmail("hai@test.com");
        this.request.setPassword("hai12345");
        assertNotEquals("test@test.com", this.request.getEmail(), "Fail Test setEmail");
        assertNotEquals("test12345", this.request.getPassword(), "Fail Test setPassword");
    }
}
