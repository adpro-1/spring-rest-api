package siben.application.siben.user.payloads;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class AuthenticationResponseTest {
    private AuthenticationResponse authResponse;

    @BeforeEach
    public void setUp() throws Exception {
        this.authResponse = new AuthenticationResponse("12345", "test");
    }

    @Test
    public void testGetMethod() {
        assertEquals("test", this.authResponse.getUsername(), "Fail Test getUsername");
        assertEquals("Bearer", this.authResponse.getTokenType(), "Fail Test getTokenType");
        assertEquals("12345", this.authResponse.getAccessToken(), "Fail Test getAccessToken");
    }

    @Test
    public void testSetMethod() {
        this.authResponse.setTokenType("Token");
        assertNotEquals("Bearer", this.authResponse.getTokenType(), "Fail Test setTokenType");
    }
}
