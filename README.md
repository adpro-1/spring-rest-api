# siBen REST API
[![pipeline status](https://gitlab.com/adpro-1/spring-rest-api/badges/master/pipeline.svg)](https://gitlab.com/adpro-1/spring-rest-api/-/commits/master)
[![coverage report](https://gitlab.com/adpro-1/spring-rest-api/badges/master/coverage.svg)](https://gitlab.com/adpro-1/spring-rest-api/-/commits/master)
## Tugas Kelompok Adprog Kelompok 1
- Alwan Harrirst Surya Ihsan (1806205483)
- Anggardha Febriano (1806235800)
- Athiya Fatihah Akbar (1806186824)
- Muhammad Nurkholish (1806186881)
- Manuel Yoseph Ray (1806141302)

## How to Run It
#### Install gradle first
``$ cd  siben``

``$ gradle bootRun``

